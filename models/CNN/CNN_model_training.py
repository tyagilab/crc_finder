import pickle
import re
import pandas as pd
import numpy as np
from tqdm import tqdm
from collections import Counter
from sklearn.model_selection import train_test_split

print('Starting to load data...')
training_data = pd.read_csv('./NewDATA_CNN_training_data.csv')
print('Data load done...')

def imagify(bs):
    img_height = 4
    img_width = 10
    bs_image = []
    for ix in range(img_height):
        bs_image.append([0] * img_width)
        
    print(bs_image)


img_height = 4
img_width = 60

def imagify(bs):

    bs_image = []
    for ix in range(img_height):
        bs_image.append([0] * img_width)

    bs_len = len(bs)
    offset = (img_width / 2 - bs_len/2)
    if bs_len % 2 == 0: 
        limit = offset + bs_len - 1
    else:
        limit = offset + bs_len

    bs_ix = 0
    for ix in range(img_width):
        if ix < offset or ix > limit:
            continue
        else:
#             print(ix)
            if bs[bs_ix] == 'a':
                bs_image[0][ix] = 1
            elif bs[bs_ix] == 'c':
                bs_image[1][ix] = 1
            elif bs[bs_ix] == 'g':
                bs_image[2][ix] = 1
            elif bs[bs_ix] == 't':
                bs_image[3][ix] = 1

            bs_ix += 1

    return bs_image


def data_prep(data):
    
    img_height = 4
    data_imgs = []
    data_labels = []

    for ix, row1 in tqdm(data.iterrows()):
        bs1 = row1['left_full_sequence_bs']
        bs2 = row1['right_full_sequence_bs']
        binding = row1['binding']

        bs1_image = imagify(bs1.lower())
        bs2_image = imagify(bs2.lower())

    #     bs1_image.append([0] * img_width)
        for ix in range(img_height):
            bs1_image[ix].extend(bs2_image[ix])

        data_imgs.append(bs1_image)

        if binding == 'co':
            data_labels.append(1)
        else:
            data_labels.append(0)
    
    return data_imgs, data_labels

print('Data prep started..')

data_imgs, data_labels = data_prep(training_data)

print(len(data_imgs))
print(len(data_labels))

batch_size = 32
img_height = 4
img_width = img_width * 2

print('Train-test split next...')
X_train, X_test, y_train, y_test = train_test_split(data_imgs, data_labels, test_size = 0.2, random_state = 4)

print('Train-test split done..\nExpanding dimensions for train...')
X_train = np.array(X_train).reshape(-1, 4, 120, 1)
print('Expanding dimensions for test...')
X_test = np.array(X_test).reshape(-1, 4, 120, 1)

y_train = np.array(y_train)
y_test = np.array(y_test)

print('Saving date into pickle')
all_vals = {}
all_vals['X_train'] = X_train
all_vals['X_test'] = X_test
all_vals['y_train'] = y_train
all_vals['y_test'] = y_test

with open('CNN_train_test.pkl', 'wb') as handle:
    pickle.dump(all_vals, handle, protocol=pickle.HIGHEST_PROTOCOL)



print('DONE')
