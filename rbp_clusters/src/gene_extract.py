import re
import pandas as pd

# filename = '/Users/tarunbonu/Tarun/sem_4/minor_thesis/deep_emscan/data/Homo_sapiens_sample_100.gtf'
# filename = '/home/tbon0008/lz25_scratch/tbon/emscan_pipeline/data/Homo_sapiens_sample_100.gtf'
filename = '/home/tbon0008/lz25_scratch/gencode.v38.annotation.gtf'

with open(filename, 'r') as fp:
    gene_data_df = pd.DataFrame(
        columns=['chromosome','gene_name', 'gene_id', 'gene_biotype', 'gene_transcript_id', 'position_start', 'position_end', 'gene_length', 'gene_source', 'strand'])
    df_ix = 0
    
    line = fp.readline()
    cnt = 1
    
    while line:
        line_elements = line.split('\t')
        if len(line_elements) > 1 and line_elements[2] == 'gene':
            try:
                gene_id = re.search(r'gene_id "(.*?)"', line_elements[8]).group(1)
            except:
                gene_id = "NA"
            try:
                gene_name = re.search(r'gene "(.*?)"', line_elements[8]).group(1)
            except:
                gene_name = "NA"
            try:
                gene_source = re.search(r'gene_source "(.*?)"', line_elements[8]).group(1) 
            except:
                gene_source = "NA"
            try:
                gene_biotype = re.search(r'gene_type "(.*?)"', line_elements[8]).group(1)
            except:
                gene_biotype = "NA"
            try:
                gene_transcript_id = re.search(r'transcript_id "(.*?)"', line_elements[8]).group(1)
            except:
                gene_transcript_id = "NA"
                
                
        
            gene_data_df.loc[df_ix] = [line_elements[0], gene_name, gene_id, gene_biotype, gene_transcript_id, line_elements[3], line_elements[4], int(line_elements[4])-int(line_elements[3]),gene_source, line_elements[6]]
            df_ix += 1
            
#             print(gene_biotype, gene_transcript_id)
#             print(line_elements[8])
            
        line = fp.readline()
        cnt += 1
#         if cnt == 10:
#             break
        
gene_data_df.to_csv('./gencode_v38_gene_data.csv', index = False)

