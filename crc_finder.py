import sys
import optparse

from helper_functions import *


if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="python crc_finder.py")
    parser.add_option('-m', '--motifs', action='store',
                      dest='input_file', help='The BED filepath for the motifs to be predicted into CRCs')
    parser.add_option('-b', '--output_bed', action='store',
                      dest='output_file_bed', help='The output BED filepath to save the predicted putative CRCs')
    parser.add_option('-c', '--output_csv', action='store',
                      dest='output_file_csv', help='The output CSV filepath to save the predicted putative CRCs')


    (args, _) = parser.parse_args()
    output_filepath_bed = args.output_file_bed
    output_filepath_csv = args.output_file_csv

    if args.input_file == None:
        print("Missing required arguments")
        sys.exit(1)

    else:

        input_file = args.input_file
        print('Loading input file - ', input_file )
        data_df = generate_combinatorial_pairs(input_file)
        print('Combinatorial motif pairs generated - ', len(data_df) )
        data_df = extract_features(data_df)
        print('Features extracted...')
        data_df_scaled = feature_transformation(data_df.copy())
        print('Features scaled for predictions...')
        predictions_df = predict_pairs(data_df_scaled, data_df)
        print('Predictions performed...')
        generate_crc(predictions_df, output_filepath_bed, output_filepath_csv)
        print('Output bed file generated - ', output_filepath_bed)
        print('Output csv file generated - ', output_filepath_csv)


