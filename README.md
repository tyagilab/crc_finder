## CRC Finder

DNA motifs are short recurring patterns on the DNA which have biological significance. These motifs work in collabortion to regulate gene expression. We call these motifs co-regulatory motifs and the group of motifs which work together are called Co-Regulatory Clusters (CRCs). A deep learning approach has been developed to identify putative CRCs given a set of motif inputs. The input to this tool is a BED file with motif occurrences and the output is a BED file with putative CRCs which can be visualised in browsers such as IGV.


<img src="./.images/Workflow.png">

---

## System Requirements

### Hardware Requirements
A standard computer with a minimum of 4 GB RAM

### Software Requirements

Python 3.5 or more


## Usage

### Load the virtual environment with pre-installed libraries
`source .virtual_env/bin/activate`

### To obtain predicted putative CRCs
`python3 crc_finder.py -m <input_filepath> [-o <output_filepath>]`

Please follow example input file format - input_motifs.bed

#### Example command
`python3 crc_finder.py -m ./input_motifs.bed`
